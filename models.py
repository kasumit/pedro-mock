from sqlalchemy import Column, Integer, String, Float, DateTime
from database import Base
from sqlalchemy.sql import func
import ast
class Order(Base):

    __tablename__ = "orders"

    id = Column(Integer, primary_key=True, autoincrement=True)
    items = Column(String)
    amount = Column(Float)
    currency = Column(String)
    installments = Column(Integer)
    customer = Column(String)
    order_id = Column(String)
    billing = Column(String)
    shipping = Column(String)
    meta = Column(String)
    options = Column(String)
    pay = Column(String)
    created_at = Column(DateTime, server_default=func.now())
    updated_at = Column(DateTime, server_onupdate=func.now())

    def __init__(self, order_id="", items=[], amount=0, currency="", installments=1, customer={}, billing={}, shipping={}, meta={}, options={}, pay={}):
        self.order_id = order_id
        self.amount = amount
        self.currency = currency
        self.customer = str(customer)
        self.items = str(items)
        self.billing = str(billing)
        self.shipping = str(shipping)
        self.meta = str(meta)
        self.options = str(options)
        self.pay = str(pay)
        self.installments = installments

    @property
    def serialize(self):
        return dict(id=self.id,
                    amount=self.amount,
                    currency=self.currency,
                    installments=self.installments,
                    customer=ast.literal_eval(self.customer),
                    items=ast.literal_eval(self.items),
                    billing=ast.literal_eval(self.billing),
                    shipping=ast.literal_eval(self.shipping),
                    meta=ast.literal_eval(self.meta),
                    options=ast.literal_eval(self.options),
                    paid=ast.literal_eval(self.pay),
                    status="completed",
                    created_at=self.created_at,
                    modified_at=self.updated_at,
                    order_id=self.order_id,
                    charged=dict(amount=self.amount, currency=self.currency)

                    )
