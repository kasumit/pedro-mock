from flask import Flask
from database import init_db, db_session
from flask import request, make_response, jsonify
from models import Order
import security

app = Flask(__name__)

init_db()


@app.route('/orders', methods=["POST"])
@security.token_required
def orders():
    req_data = request.get_json(silent=True)
    items = req_data['items']
    currency = req_data['currency']
    order_id = req_data['order_id']
    billing = req_data['billing']
    amount = req_data['amount']
    installments = req_data['installments']
    shipping = req_data['shipping']
    meta = req_data['meta']
    options = req_data['options']
    pay = req_data['pay']
    customer = req_data['customer']
    if pay is not None:
        if pay.get("with") is None:
            return jsonify(error=dict(type="order", code="1001", message="enter a parameter for pay.with (ex: gateway)")), 404
        elif pay.get('data') is None:
            return jsonify(error=dict(type="order", code="1002", message="pay.data cannot be null add payment parameters")), 404
        elif pay.get("data").get("name") == 'paycell' and (pay.get("data").get("parameters") is None or pay.get("data").get("parameters").get("card_token") is None):
            return jsonify(error=dict(type="order", code="1003", message="pay.data.parameters cannot be empty for paycell, send the phone_number")), 404

        elif pay.get("data").get("name") == 'paycell' and ( pay.get("data").get("parameters") is None or pay.get("data").get("parameters").get("phone_number") is None):
            return jsonify(error=dict(type="order", code="1003",
                                      message="pay.data.parameters cannot be empty for paycell, send the card_token")), 404

    ord = Order(
        order_id=order_id,
        items=items,
        billing=billing,
        shipping=shipping,
        meta=meta,
        options=options,
        pay=pay,
        amount=amount,
        installments=installments,
        currency=currency,
        customer=customer
    )
    db_session.add(ord)
    db_session.commit()
    return jsonify(ord.serialize)


@app.route('/orders/<int:id>', methods=['GET'])
@security.token_required
def order(id):
    order = Order.query.filter_by(id=id).first()
    if order is None:
        return jsonify(error=dict(type="order", code="1004",
                       message="order is not found")), 404
    return jsonify(order.serialize)


if __name__ == '__main__':
    app.run(debug=True)



