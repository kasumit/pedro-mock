from functools import wraps
from flask import request, jsonify


default_tokens =[
    "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJuYW1lIjoiSm9obiBEb2UifQ.xuEv8qrfXu424LZk8bVgr9MQJUIrp1rHcPyZw_KSsds"
]
print(default_tokens)
#jwt.decode(encoded, 'secret', algorithms=['HS256'])



def token_required(f):
    @wraps(f)
    def decorated(*args, **kwargs):
        token = None
        #auth = request.get_json(silent=True)
        if "authorization" in request.headers:
            token = request.headers.get('authorization')
            token = str(token).split("Bearer ")[1]
        if not token:
            return jsonify(message="Token is not found"), 401


        if token in default_tokens:
            return f(*args, **kwargs)
        else:
            return jsonify(message="Token is invalid"), 401
    return decorated
